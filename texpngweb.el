;; Sample emacs configuration

(if (eq system-type 'android)
  (progn ;; Android custom org-latex-preview
    (setq org-format-latex-options (plist-put org-format-latex-options :scale 100))
    (setq org-preview-latex-process-alist
          '((dvipngweb
              :programs ("curl")
              :description "dvi > png"
              :message "you need to install the programs: curl."
              :image-input-type "dvi"
              :image-output-type "png"
              :image-size-adjust (1.0 . 1.0)
              :latex-compiler ("curl -F \"file=@%f\" -F \"type=tex\" 0.0.0.0:8000 && curl 0.0.0.0:8000/tmp.dvi -o %O")
              :image-converter ("curl -F \"file=@%f\" -F \"type=dvi\" -F \"dpi=%D\" -o %O 0.0.0.0:8000 && curl 0.0.0.0:8000/tmp.png -o %O"))))
    (setq org-latex-create-formula-image-program 'dvipngweb)))
