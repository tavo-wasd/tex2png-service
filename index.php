<?php
$host = $_SERVER['HTTP_HOST'];
$type = $_POST['type'];

if ($type == "tex") {
  move_uploaded_file($_FILES["file"]["tmp_name"], "tmp.tex");
  $output = shell_exec("latex -interaction nonstopmode tmp.tex");
  echo "http://$host/tmp.dvi";
}

if ($type == "dvi") {
  $dpi = isset($_POST['dpi']) ? intval($_POST['dpi']) : 200;
  move_uploaded_file($_FILES["file"]["tmp_name"], "tmp.dvi");
  shell_exec("dvipng -D $dpi -T tight -bg Transparent -o tmp.png tmp.dvi");
  echo "http://$host/tmp.png";
}

?>
